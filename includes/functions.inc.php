<?php

// Returns a shuffled array using the Fisher-Yates method.
// Taken from the PHP Cookbook.
function t_pc_array_shuffle($array) {
    $i = count($array);

   if($i > 0)
	{
		while(--$i) {
			$j = mt_rand(0, $i);
	
			if ($i != $j) {
				// swap elements
				$tmp = $array[$j];
				$array[$j] = $array[$i];
				$array[$i] = $tmp;
			}
    	}
	}

    return $array;
}

// Returns the content of the card.
function t_display_card($set_id, $card_id, $nodetype){
	$table = strtolower($nodetype)."_data";
	$result = db_query("SELECT front, imageurl, back
				FROM {".$table."}
				WHERE id = '$card_id'"); 
	if($result)
	$row = mysql_fetch_assoc($result);
	
	//filter according to nodetype
	switch($nodetype){
		case "fitb":		
		$row['form'] = t_filter($row['back']);//"filtered something";
		$row['formname'] = "ans";
		break;
		case "qanda":
		$row['form'] = t_filter_qanda($row['back']);//"highlighted words something";
		$row['formname'] = "ans";
		break;
		default: //just return an answer form
		$max = 4;
		$str = "";
		while($max--)
		{
			$str.=chr(rand(97, 122));
		}
		$row['form'] = '<input type="text" name="'.$str.'" id="ans">
						<input type="hidden" name="blanks" value="'.$str.'"><br>';
		$row['formname'] = "ans";
		break;
	}
	
	return $row;
}


function t_filter($text)
{
	$text =  preg_replace_callback("/\[{2}([A-Za-z0-9]*)\]{2}/", "doR", $text);
	$_blanks = $_GET["blanks"];
	
	preg_match_all("/value=\'.*\'/", $text, $matches);
	
	//replace non-matching values with blank spaces
	$matches = $matches[0];
	for($i = 0; $i < sizeof($matches); $i++)
	{
		if(strcmp($matches[$i], "value='".$_GET[($_blanks[$i])]."'"))
		{
			$text = str_replace($matches[$i], "", $text);
		}
	}
	
	return $text;
}

//Create multichoice
function t_filter_qanda($text){	
	foreach(split("\n", $text) as $key=>$opt) {
		if($opt){
			$options[] = preg_replace("/\[{2}|\]{2}/", "", $opt);
			if(substr($opt, 0, 2)=='[[') $correct[$key]=$key;
		}
	}
	//only keep the correct answers
	$form['blanks'] = array(
			'#type' => 'checkboxes',
			'#prefix' => '<div class="card-options">',
  			'#suffix' => '</div>', 
			'#options' => $options,
			'#default_value' => (is_array($correct)&& is_array($_GET['edit']["blanks"]))?array_intersect($correct, $_GET['edit']["blanks"]):array(),
			'#description'=>'');
	$text = form_render(form_builder(NULL, $form));
	return $text;
}


function doR($s)
{	
	$max = 4;
	$str = "";
	$formnames = array();
	while($max--)
	{
		$str.=chr(rand(97, 122));
	}
	array_push($formnames, $str);
	
	$rem = "<input type=text id='ans' name=".$str." size=".strlen($s[1])." value='".$s[1]."'>";
	return "<input type=hidden name=blanks[] value=".$str.">\n".$rem;
}

function t_check_answer($nodetype, $card_content, $view, $display_order_size, $flip_side="front"){
	$blanks = $_GET['blanks'];
	$_blanks = array(); 
	//Check answers
	switch($nodetype){
		case 'qanda':
		$blanks = $_GET['edit']['blanks'];
		foreach(split("\n", $card_content['back']) as $key=>$opt) {
		if($opt){
				if(substr($opt, 0, 2)=='[[') {
					$_blanks[$key]=$key;
					$_options[]= preg_replace("/\[{2}|\]{2}/", "", $opt);
				}
			}
		}
		if($_blanks == $blanks) {
			$correct = 1;			
			$correct_ans = is_array($_options)? nl2br(join($_options, "\n")):t('No correct answer specified');
		} 
		break;
		case "fitb":{
		
			for($i=0; $i < sizeof($blanks); $i++){
				array_push($_blanks, $_GET[($blanks[$i])]);
			}

			$userAns =  preg_replace_callback("/\[{2}([A-Za-z0-9]*)\]{2}/", "doR2", $card_content['back']);
			preg_match_all("/\[{2}([A-Za-z0-9\s]*)\]{2}/", $card_content['back'], $uAns ); 
			$uAns = $uAns[1];

			for($i = 0, $j = 0; $i < sizeof($uAns); $i++)
			{//check all words within [[  ]] with a $dbaseAns3			
				if((array_key_exists($i, $uAns)) && ($_blanks[$i] == $uAns[$i])){
					$j++;
					$output.='<span class="card-filter">'.t('Correct Answer - Position').($i+1).'</span>';					
				}
			}

			if($j == $i)	{
				$side = $flip_side;			
				$correct_ans = preg_replace("/\[{2}|\]{2}/", "", $card_content['back'])."\n";
				$correct = 1;
			}
		}
		break;		
		default:
		{
			$blanks = $_GET['blanks'];
			$uAns = array($card_content[t_swap($flip_side)]);
			if(trim(strtolower($_GET[$blanks])) == strtolower($card_content[t_swap($flip_side)])){
			//answer correct. move to next card				
				$side = $flip_side;
				$correct_ans.= $_GET[$blanks];				
			}
		}
		break;
	}

	//Display Result
	switch($nodetype){
		case 'fitb':
		case 'vocab':
	if($flip_side == $side){ //sides changed, so correct	
		if($view+1 < $display_order_size){				
			 $correct_ans.= ("<br><span class='card_back'>
			 <form name='next' method='get' id='next' action=''>
			 <input name='but' type=submit value='".t('Next')."'>
			 <input type=hidden name='view' value=".trim($view+1).">
			 <input type='hidden' name='q' value='".$_GET['q']."'>
			 </form></span>"); 
			 $correct_ans.= "<script>document.next.but.focus();</script>";
		}
		$output.= '<div align="center" class="card_back">'.$correct_ans.'</div>'; //card_content[$side];
	}
	else
	{
		for($i = 0; $i < sizeof($uAns); $i++){ 
			$hintbuttons .= '<input type="button" name="hint'.$i.'" value="'.t('Hint').'" onclick="hint('.$i.',\'ans\')">';
		}		
		$output.= '<form  id="tryForm" name="tryForm" method="get">';
		$output.='<div align="center">'.$card_content['form'].'</div>';				
		$output.='<div align="center">'.$hintbuttons.'</div>';
		$output.='<div align="center"><input type="submit" value="'.t('Check Answer').'"></div>
		<input type="hidden" name="view" value="'.$view.'">
		<input type="hidden" name="q" value="'.$_GET['q'].'">
		<input type="hidden" name="ss" value="'.$_GET['ss'].'">
		</form>
		<script language="javascript" type="text/javascript">
		<!--
			var obj = document.tryForm.'.$card_content['formname'].';
			if(obj.length){
				obj = obj[0].name;
			}	else 	{
				obj = obj.name;
			}
			eval("document.tryForm."+obj+".focus()");
		//-->
		</script>';
	}
	break;	
	case 'qanda':
	if($correct){	
		if($view+1 < $display_order_size){				
			 $correct_ans.= ("<br><span class='card_back'>
			 <form name='next' method='get' id='next' action=''>
			 <input name='but' type=submit value='".t('Next')."'>
			 <input type=hidden name='view' value=".trim($view+1).">
			 <input type='hidden' name='q' value='".$_GET['q']."'>
			 </form></span>"); 
			 $correct_ans.= "<script>document.next.but.focus();</script>";
		}
		$output.= '<div align="center" class="card_back">'.$correct_ans.'</div>'; //card_content[$side];
		
	}	else	{
		
		$output.= '<form  id="tryForm" name="tryForm" method="get">';
		$output.='<div align="center">'.$card_content['form'].'</div>';				
		$output.='<div align="center">'.$hintbuttons.'</div>';
		$output.='<div align="center"><input type="submit" value="'.t('Check Answer').'"></div>
		<input type="hidden" name="view" value="'.$view.'">
		<input type="hidden" name="q" value="'.$_GET['q'].'">		
		</form>
		';
	}
	
	break;
	}
	return array('value'=>$output, 'correct'=>$correct);
}


function t_display_actions($view, $display_order_size, $flip_side)
{
	$html.='<table cellpadding="1" cellspacing="1" width="90%" border="0" align="center">
			<tr>';
			if($view !=0)
			{
				$html.= '<td class="action" align="center">
						<P class="action"><a class="action" href="?q='.$_GET['q'].'&view=0&ss='.$_GET['ss'].'">|&lt; '.t('First').'</a>
						</td>
						<td class="action" align="center">
							<P class="action"><a class="action" href="?q='.$_GET['q'].'&view='. ($view-1).'&ss='.$_GET['ss'].'">&lt; '.t('Previous').'</a></P>
							</td>';
			}
			else
			{
				$html.=	'<td class="action" align="center">
								<P class="action">|&lt; '.t('First').'</P>
							</td>
							<td class="action" align="center">
								<P class="action">&lt; '.t('Previous').'</P>
						</td>';
					
			}
			
			/*	$html.= '<td class="action" align="center">
							<P class="action">
								<a class="action" href="?q='.$_GET['q'].'&view='.$view.'&side='.$flip_side.'&ss='.$_GET['ss'].'">Geuza</a>
							</P>
						</td>'; */
						
			if($view+1 < $display_order_size)
			{
					$html.= '<td class="action" align="center">
								<P class="action"><a class="action" href="?q='.$_GET['q'].'&view='.($view+1).'&ss='.$_GET['ss'].'">'.t('Next').' &gt;</a></P>
							</td>';
					$html.='<td class="action" align="center">
								<P class="action"><a class="action" href="?q='.$_GET['q'].'&view='.($display_order_size-1).'&ss='.$_GET['ss'].'">Last &gt;|</a></P>
							</td>';
			}
			else
			{
					$html.= '<td class="action" align="center">
								<P class="action">'.t('Next').' &gt;</P>
							</td>
							<td class="action" align="center">
								<P class="action">Last &gt;|</P>
							</td>';
			}
			$html.= '</table>';
			return $html;
}

function t_display_quick_actions($view, $display_order_size, $flip_side){
	$html = '<span class="NavMenuBody">			
	<table border="0" cellpadding="0" cellspacing="0" class="subText">
	  <tr>
		<td><span class="NavMenu"><b>'.t('Quick Navigation').'</b> &lt;'; 
	if($view+1 < $display_order_size)
	{
		$html.= '<a href="?q='.$_GET['q'].'&view='.($view+1).'&ss='.$_GET['ss'].'">'.t('Next Card').'</a> &gt;&lt;';
	}
	else
	{
	}

	if($view !=0)
	{
		$html.= '<a href="?q='.$_GET['q'].'&view='.($view-1).'&ss='.$_GET['ss'].'">'.t('Previous Card').' </a> &gt;&lt; ';
    }
	else
	{
	}
	$ss = ($_GET['ss']=='1')? '0' : '1';
	$html.= ' <a href="?q='.$_GET['q'].'/options">'.t('Options').'</a>&gt;
	</span></td></tr></table></span>';
	return $html;
}


function t_swap($flip_side)
{
 if($flip_side == "front")
 return "back";
 else
 return "front";
}
?>
